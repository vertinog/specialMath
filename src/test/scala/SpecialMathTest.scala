import org.scalatest.{FunSuite, Matchers}

class SpecialMathTest extends FunSuite with Matchers {
  val testTable: Array[(Int, BigInt)] = Array(
    (-1000, 0),
    (-1, 0),
    (0, 0),
    (1, 1),
    (2, 3),
    (3, 7),
    (4, 14),
    (5, 26),
    (6, 46),
    (7, 79),
    (8, 133),
    (9, 221),
    (10, 364),
    (11, 596),
    (12, 972),
    (13, 1581),
    (14, 2567),
    (15, 4163),
    (16, 6746),
    (17, 10926),
    (18, 17690),
    (19, 28635),
    (20, 46345),
    (90, BigInt.apply("19740274219868223074", 10))
  )

  for ((input, expected) <- testTable) {
    test(s"encode should return $expected when passed $input") {
      SpecialMath.specialMath(input) shouldEqual expected
    }
  }

}
