object SpecialMath {
  lazy val specialMathTailRecStream: Stream[BigInt] = {
    def loop(n: BigInt, a: BigInt, b: BigInt): Stream[BigInt] = a #:: loop(n + 1, b, n + a + b)
    loop(2, 0, 1)
  }

  def specialMath(input: Int): BigInt = specialMathTailRecStream.drop(input)(0)

  def main(args: Array[String]): Unit = {
    if (args.length != 1) {
      println("Expecting a single argument")
    } else if (args(0).toInt < 0) {
      println("Expecting a positive integer as an argument")
    } else {
      try {
        println(specialMath(args(0).toInt))
      } catch {
        case nfe: NumberFormatException => println("Expecting an integer as an argument")
      }
    }
  }
}
